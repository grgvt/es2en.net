#! /usr/bin/env python3.12
import io
import os
import logging
import socketserver
from http import HTTPStatus
from http.server import SimpleHTTPRequestHandler
from pathlib import Path


log_fmt = '[%(levelname)s][%(funcName)s %(lineno)d] %(message)s'
logging.basicConfig(level=logging.INFO, format=log_fmt)
log = logging.getLogger(__name__)
PORT = 8123
ROOT_DIR = Path(__file__).parent.joinpath('public')


class RequestHandler(SimpleHTTPRequestHandler):
    def __init__(self, *args, directory=ROOT_DIR, **kwargs):
        super().__init__(*args, directory=directory, **kwargs)

    def do_GET(self):
        """
        Sets Content-Type header to text/html when there is no file extension
        """
        if not self.path or self.path == '/':
            p = self.translate_path('index.html')
        else:
            p = self.translate_path(self.path)

        f = None
        fs = None

        if os.path.exists(p) and os.path.isfile(p):
            f = open(p, 'rb')
            fs = os.fstat(f.fileno())
            self.send_response(HTTPStatus.OK)
        else:
            log.error(f'file doesn’t exist: {p}')
            self.send_error(HTTPStatus.NOT_FOUND, 'file not found')

        fn, ext = os.path.splitext(p)
        # assumes HTML for paths with no extension
        if not ext:
            ctype = 'text/html; charset=utf-8'
        else:
            ctype = self.guess_type(p)

        # set headers
        self.send_header('Content-Type', ctype)
        if fs:
            self.send_header('Content-Length', str(fs[6]))
        self.end_headers()

        # finish
        try:
            if f:
                self.copyfile(f, self.wfile)
        except Exception as Ex:
            self.send_response(HTTPSTatus.INTERNAL_SERVER_ERROR)
            log.error(Ex)
        finally:
            if f:
                f.close()


def main():
    handler = RequestHandler
    socketserver.TCPServer.allow_reuse_address = True

    with socketserver.TCPServer(('', PORT), handler) as httpd:
        log.info(f'serving on port {PORT}')
        try:
            httpd.serve_forever()
        except Exception as Ex:
            log.error(Ex)
        finally:
            httpd.server_close()


if __name__ == '__main__':
    main()
