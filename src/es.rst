Gregory Vigo Torres
===================

.. container:: centered italic

   Traductor español inglés |mdash| Desde 2007

Traduzco todo tipo de *documentos generales* y puedo *adaptar sus textos* cuando una traducción literal no sea adecuada.
Durante los más de 15 años de mi trayectoria profesional he traducido diversos textos desde manuales y notas de prensa hasta páginas web turísticas, cartas para restaurantes y textos para catálogos de exhibiciones.
Asimismo, he creado, editado y traducido *subtítulos* en inglés para vídeos corporativos y documentales.

Tengo conocimientos especializados en el *arte*, la *informática* y el *turismo*.
Mis títulos universitarias son: el grado en bellas artes (bachelor’s degree) del School of The Art Institute of Chicago y un máster de bellas artes de la Universidad de Pennsylvania.
Soy aficionado del software libre desde hace más de 15 años y de la programación en Python y JavaScript desde hace más de 10 años.
Soy nativo en inglés de los EEUU y llevo más de 20 años en España.



`Descarga mi cv`_

.. _Descarga mi cv: static/gregoryvigotorres_cv.pdf


.. include:: <xhtml1-special.txt>
