Gregory Vigo Torres
===================


.. container:: centered italic

   Spanish to English Translator |mdash| Since 2007

**I can** translate all kinds of *general documents*  or *adapt texts*  when a literal translation doesn’t suit your needs.
In my over 15 years of experience, I’ve translated a wide range of texts including manuals, press releases, tourism websites, restaurant menus, texts for exhibition catalogs and more.
And I’ve created, edited and translated *subtitles* for documentaries and corporate videos.

My main areas of expertise are *art*, *IT* and *tourism*.
I have a bachelor’s degree from The School of The Art Institute of Chicago and a master’s of fine arts degree from the University of Pennsylvania.
I’ve been a free software enthusiast for over 15 years and I’ve been coding with Python and JavaScript for more than 10. I’m a native English speaker from the US and I’ve lived in Spain for more than 20 years.



`Download my CV`_

.. _Download my CV: static/gregoryvigotorres_cv.pdf


.. include:: <xhtml1-special.txt>
