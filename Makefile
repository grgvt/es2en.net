.RECIPEPREFIX = >
DIST_ROOT = ./public

all : clean html css copystatic

html :
> ./buildcmds compilehtml

css :
> ./buildcmds compilesass

copystatic :
> ./buildcmds copystaticfiles

clean :
> rm -rf "$(DIST_ROOT)"/*

watch :
> ./buildcmds watch

serve :
> python3.12 ./server.py

publish :
> echo "build and push to gitlab"
# git checkout master && git push gitlab master
